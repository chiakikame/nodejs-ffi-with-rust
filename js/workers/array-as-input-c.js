/* eslint-disable no-unused-vars */
const logger = require('../lib/logger')('array-as-input-c');
const ref = require('ref');
const arrType = require('ref-array');
const Uint32Array = arrType('uint32');

require('../lib/run-library-plugin')(process.argv[2], {
  'sum_arr_c': ['uint32', [Uint32Array, 'size_t']]
}, function runStrangeAdd(libd) {
  const arr = new Uint32Array(10);
  for (let i = 0; i < 10; i++) {
    arr[i] = i + 1;
  }
  logger.info(`sum_arr_c returns ${libd.sum_arr_c(arr, arr.length)}`);
});
