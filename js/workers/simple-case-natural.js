const logger = require('../lib/logger')('simple-case-natural');

require('../lib/run-library-plugin')(process.argv[2], {
  'strange_add_natural': ['uint32', ['uint32', 'uint32']]
}, function runStrangeAddNatural(libd) {
  logger.info(`strange_add_natural(30, 40) yields ${libd.strange_add_natural(30, 40)}`);
});
