const logger = require('../lib/logger')('simple-case-c');

require('../lib/run-library-plugin')(process.argv[2], {
  'strange_add_c': ['uint32', ['uint32', 'uint32']]
}, function runStrangeAdd(libd) {
  logger.info(`strange_add_c(30, 40) yields ${libd.strange_add_c(30, 40)}`);
});
