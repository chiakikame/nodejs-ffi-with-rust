/* eslint-disable no-unused-vars */
const logger = require('../lib/logger')('array-as-input-natural');
const ref = require('ref');
const arrType = require('ref-array');
const Uint32Array = arrType('uint32');

require('../lib/run-library-plugin')(process.argv[2], {
  'sum_arr_natural': ['uint32', [Uint32Array, 'size_t']]
}, function runStrangeAddNatural(libd) {
  const arr = new Uint32Array(10);
  for (let i = 0; i < 10; i++) {
    arr[i] = i + 1;
  }
  
  logger.info(`sum_arr_natural returns ${libd.sum_arr_natural(arr, arr.length)}`);
});
