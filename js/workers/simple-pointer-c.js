const logger = require('../lib/logger')('simple-pointer-c');
const ref = require('ref');

const endianess = require('os').endianness();
// Reader name for endianess of current machine
const uint32Reader = `readUInt32${endianess}`;
const uint32Writer = `writeUInt32${endianess}`;
const uintptr = ref.refType('uint32');

require('../lib/run-library-plugin')(process.argv[2], {
  'strange_add_in_place_c': ['void', [uintptr, uintptr, uintptr]]
}, function runStrangeAddInPlace(libd) {
  const a = ref.alloc('uint32');
  a[uint32Writer](12, 0);
  const b = ref.alloc('uint32');
  b[uint32Writer](23, 0);
  const c = ref.alloc('uint32');
  c[uint32Writer](0, 0);
  libd.strange_add_in_place_c(a, b, c);
  logger.info(`s_a_i_c(12, 23, c), c returns ${c[uint32Reader](0)}`);
});
