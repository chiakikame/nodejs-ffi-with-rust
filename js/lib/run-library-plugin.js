//
// Load the library via FFI package and trying to use the functions in
// the shared object.
//
const ffi = require('ffi');
const logger = require('./logger')('runner');

module.exports = function runLibraryPlugin(libraryPath, apiObj, funcToRun) {
  logger.info(`loading library ${libraryPath}`);
  
  let lib = ffi.Library(libraryPath, apiObj);
  funcToRun(lib);
};
