//
// Run "cargo build"
//
const execFile = require('child_process').execFile;
const logger = require('./logger')('cargo');

/* eslint-disable no-unused-vars */
module.exports = function runCargoBuild(rustProjectDir) {
  return new Promise(function runCargoBuildInternal(resolve, reject) {
    logger.info('Running "cargo build"');
    execFile('cargo', ['build', '--release'], {
      cwd: rustProjectDir
    }, function cargoFinished(err, stdout, stderr) {
      if (err) {
        logger.fatal('cargo build" failed');
        reject(err);
      } else {
        logger.info('build finished');
        resolve();
      }
    });
  });
};
