//
// Name of dynamic-linked library by base name
//
const os = require('os');

module.exports = function dynObjName(base) {
  return {
    Linux: `lib${base}.so`,
    Windows_NT: `${base}.dll`,
    Darwin: `lib${base}.dylib`
  }[os.type()] || base;
};
