const fork = require('child_process').fork;
const fs = require('fs');
const path = require('path');
const logger = require('./logger')('worker-scheduler');

module.exports = function runAllWorkers(libraryPath, workerDir) {
  fs.readdir(workerDir, function ls(err, files) {
    if (err) {
      logger.fatal(`${err}`);
    } else {
      processFiles(libraryPath, workerDir, files);
    }
  });
};

function processFiles(libraryPath, workerDir, files) {
  if (files.length === 0) {
    logger.info('All job run.');
    return;
  }
  
  const file = files.shift();
  const fullPath = path.join(workerDir, file);
  
  logger.info(`Running ${fullPath}`);
  const child = fork(fullPath, [libraryPath], {
    stdio: [process.stdin, process.stdout, process.stderr, 'ipc']
  });
  logger.info(`Run     ${fullPath} in PID ${child.pid}`);
  child.once('exit', function childExit(code, signal) {
    logger.info(`Worker exited with code ${code}, signal ${signal}`);
    processFiles(libraryPath, workerDir, files);
  });
}
