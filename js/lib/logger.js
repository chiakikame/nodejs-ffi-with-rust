//
// Always log
//

/* eslint-disable no-console */
function logger(level, tag, str) {
  console.log(`${level} | PID ${process.pid} | [${tag}] ${new Date().toISOString()} | ${str}`);
}

module.exports = function(tag = '(global)') {
  return {
    fatal: logger.bind(null, 'fail', tag),
    warn: logger.bind(null, 'warn', tag),
    info: logger.bind(null, 'info', tag),
    debug: logger.bind(null, 'dbg ', tag)
  };
};
