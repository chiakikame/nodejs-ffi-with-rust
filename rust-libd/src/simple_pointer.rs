extern crate libc;

#[no_mangle]
pub extern fn strange_add_in_place_c(
    a: *const libc::uint32_t,
    b: *const libc::uint32_t,
    c: *mut libc::uint32_t
) {
    unsafe {
        strange_add_in_place(&*a, &*b, &mut *c);
    }
}

#[no_mangle]
pub extern fn strange_add_in_place_natural(a: &u32, b: &u32, c: &mut u32) {
    strange_add_in_place(a, b, c);
}

fn strange_add_in_place(a: &u32, b: &u32, c: &mut u32) {
    *c = a + b + 70;
}
