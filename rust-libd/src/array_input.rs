extern crate libc;

use std::slice;

#[no_mangle]
pub extern fn sum_arr_c(arr: *const libc::uint32_t, elem_count: libc::size_t) -> libc::uint32_t {
    let arr = unsafe { slice::from_raw_parts(arr, elem_count as usize) };
    sum_items(arr.iter().map(|x| x as &u32)) as libc::uint32_t
}

#[no_mangle]
pub extern fn sum_arr_natural(arr: *const u32, elem_count: usize) -> u32 {
    let arr = unsafe { slice::from_raw_parts(arr, elem_count) };
    sum_items(arr.iter())
}

#[no_mangle]
pub extern fn sum_arr_natural_panicked(arr: &[u32], elem_count: usize) -> u32 {
    sum_items(arr[0 .. elem_count].iter())
}

fn sum_items<'a, T>(iter: T) -> u32
  where T: Iterator<Item = &'a u32> {
    iter.fold(0, |acc, v| {
        acc.saturating_add(*v)
    })
}
