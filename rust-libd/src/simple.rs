extern crate libc;

#[no_mangle]
pub extern fn strange_add_c(a: libc::uint32_t, b: libc::uint32_t) -> libc::uint32_t {
    strange_add_impl(a as u32, b as u32) as libc::uint32_t
}

#[no_mangle]
pub extern fn strange_add_natural(a: u32, b: u32) -> u32 {
    strange_add_impl(a, b)
}

fn strange_add_impl(a: u32, b: u32) -> u32 {
    a + b + 43
}
