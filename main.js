const cargoBuilder = require('./js/lib/cargo-build');
const logger = require('./js/lib/logger')('main');
const dynObjName = require('./js/lib/dyn-obj-name');
const runAllWorkers = require('./js/lib/run-all-workers');
const path = require('path');

const workerDir = path.join(__dirname, 'js', 'workers');

function main() {
  const rustProjectDir = path.join(__dirname, 'rust-libd');
  const dynObjPath =
    path.join(rustProjectDir, 'target', 'release', dynObjName('libd'));
  
  cargoBuilder(rustProjectDir)
    .then(function bulidSuccessful() {
      return runAllWorkers(dynObjPath, workerDir);
    }).catch(function error(err) {
      logger.fatal(`${err.name}: ${err.message}\n${err.stack}`);
    });
}

main();
