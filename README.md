# FFI in node.js with Rust

Node.js is a great platform for developing web application and fast prototyping. However, when encountering computation-intensive jobs, developers need to use a programming language with better performance, like C, C++, or Rust.

Since it's easier to populate a project Rust project than others in my opinion, in this project, I explore how to load and use a Rust dynamic library in javascript / Node.js with `ffi` npm package.

## Feature

* Compile Rust binary with `Cargo` before loading it via `ffi`
* Demonstrate how to use `ffi` (JS side)
* Demonstrate how to write a ffi-able Rust code (Rust side)

## Using FFI: mini discussion

The service in this context is, the CPU-intensive codes written in Rust.

Features:

* For the user and the service, data resides inside the same process.
* The service will be distributed with the user application as a dynamically linked library.

Advantages:

* Performance. Data (parameters) are transferred in memory space of the same process. In most cases, the system copies pointer of the data only.

Disadvantages:

* Both the user and service must adhere low-level details of FFI protocol (C calling convension, `struct`, `pointer` ...)
* Need wrapping to scale beyond a single machine

### Alternative design

To utilize both security and performance power of Rust, one can design computation-intensive part of their application as an executable capable of receiving input from stdin / file and produce output into stdout. Service consumer can utilize the service via process management libraries (which is included in the standard library of some programming languages).

Another design is to design the service as a simple microservice which provide a HTTP, TCP, UDP, or even raw socket interface. An additional advantage of this design compared to the one mentioned in the above paragraph is that you may put the service into separate machine.

## TODO

* FFI: array input
* FFI: object lifecycle

## Reference about FFI between JS and Rust

* [Official `node-ffi` tutorial](https://github.com/node-ffi/node-ffi/wiki/Node-FFI-Tutorial)
* [`ref` documentation](http://tootallnate.github.io/ref/)
* [Complex data structure with `node-ffi`](http://pixomania.net/programming/complex-data-structures-with-node-ffi/)
* Cargo guide
  * [Guide](http://doc.crates.io/guide.html)
  * [.toml format](http://doc.crates.io/manifest.html)
* The Rust Programming Language book
  * [Rust inside other languages](https://doc.rust-lang.org/1.5.0/book/rust-inside-other-languages.html)
  * [Calling Rust from C](https://doc.rust-lang.org/1.5.0/book/ffi.html#calling-rust-code-from-c)
*
